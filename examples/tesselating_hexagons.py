'''
    Renders a floating, oscillating, 3d islan with lights
'''

import os
import sys
import moderngl
import numpy as np
from objloader import Obj
from PIL import Image
from pyrr import Matrix44, Vector4, Vector3
from PyQt5.QtMultimedia import QSound


if os.path.exists("gamelib"):
    sys.path.append("gamelib")
else:
    print("Probs have to run this from the root directory ...")
    exit(-1)

import data
from window import Example, run_example


class ColorsAndTexture(Example):
    gl_version = (3, 3)
    title = "Colors and Textures"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.prog = self.ctx.program(
            vertex_shader='''
                #version 330

                uniform mat4 Mvp;

                in vec3 in_vert;
                in vec3 in_norm;
                in vec2 in_text;

                out vec3 v_vert;
                out vec3 v_norm;
                out vec2 v_text;

                void main() {
                    gl_Position = Mvp * vec4(in_vert, 1.0);
                    v_vert = in_vert;
                    v_norm = in_norm;
                    v_text = in_text;
                }
            ''',
            fragment_shader='''
                #version 330

                uniform vec3 Light;
                uniform vec3 Color;
                uniform bool UseTexture;
                uniform sampler2D Texture;

                in vec3 v_vert;
                in vec3 v_norm;
                in vec2 v_text;

                out vec4 f_color;

                void main() {
                    float lum = clamp(dot(normalize(Light - v_vert), normalize(v_norm)), 0.0, 1.0) * 0.8 + 0.2;
                    if (UseTexture) {
                        f_color = vec4(texture(Texture, v_text).rgb * lum, 1.0);
                    } else {
                        f_color = vec4(Color * lum, 1.0);
                    }
                }
            ''',
        )

        self.mvp = self.prog['Mvp']
        self.light = self.prog['Light']
        self.color = self.prog['Color']
        self.use_texture = self.prog['UseTexture']

        self.objects = {}

        for name in ['scene-1-ground', 'scene-1-grass', 'scene-1-billboard', 'scene-1-billboard-holder', 'scene-1-billboard-image']:

            obj = Obj.open(data.filepath('%s.obj' % name))
            vbo = self.ctx.buffer(obj.pack('vx vy vz nx ny nz tx ty'))
            vao = self.ctx.simple_vertex_array(self.prog, vbo, 'in_vert', 'in_norm', 'in_text')
            if name.startswith("scene-1-"):
                self.objects[name[len("scene-1-"):]] = vao
            else:
                self.objects["billboard-image"] = vao


        info_img = Image.open(data.filepath('infographic-1.jpg')).transpose(Image.FLIP_TOP_BOTTOM).convert('RGB')
        self.texture = self.ctx.texture(info_img.size, 3, info_img.tobytes())
        self.texture.build_mipmaps()

        self.pipe_textures = {}

        for i, pipe_no in enumerate(["one", "two", "three", "four", "five", "six", "seven"]):
            try:
                img = Image.open(data.filepath('%s.png' % pipe_no)).transpose(Image.FLIP_TOP_BOTTOM).convert('RGB')
            except:
                img = info_img

            self.pipe_textures[i] = self.ctx.texture(img.size, 3, img.tobytes())
            self.pipe_textures[i].build_mipmaps()

        hexagon_verts = []
        self.hex_radius = 10.0
        radius = self.hex_radius
        # centre point
        hexagon_verts.append([0, 0, 0, 0, 0, 1, 0.5, 0.5])

        for i in range(7):
            angle = 60. * i # in degrees

            hexagon_verts.append([radius*np.cos(angle * np.pi / 180.0), radius*np.sin(angle * np.pi / 180.0), 0,
                0, 0, 1, # normals
                (np.cos(angle * np.pi / 180.0)+1.)/2., (np.sin(angle * np.pi / 180.0) + 1.)/2. # tex coords
            ])

        self.hexagon_vbo = self.ctx.buffer(np.array(hexagon_verts, dtype="f4"))
        self.hexagon_vao = self.ctx.simple_vertex_array(self.prog, self.hexagon_vbo, 'in_vert', 'in_norm', 'in_text')

        # now for some audio
        # maps hex cube coordinates to waves to play
        # (0, 0, 0) is centre hex
        self.audio_map = {}
        audio_files = ["one", "two", "three"]
        cube_coordinates = [(0, 0, 0), (1, 0, -1), (0, 1, -1)]

        for audio_file, cube_coord in zip(audio_files, cube_coordinates):
            filename = data.filepath("%s.wav" % audio_file)
            if not os.path.exists(filename):
                filename = data.filepath("one.wav")
            self.audio_map[cube_coord] = QSound(filename)



    def render(self, time: float, frame_time: float):
        self.ctx.clear(1.0, 1.0, 1.0)
        self.ctx.enable(moderngl.DEPTH_TEST)

        self.proj = Matrix44.perspective_projection(60.0, self.aspect_ratio, 0.1, 1000.0)
        self.lookat = Matrix44.look_at(
            (0.0, 0.0, 50.),
            (0.0, 0.0, 0.0),
            (0.0, 1.0, 0.0),
        )

        #rotate = Matrix44.from_z_rotation(np.sin(time) * 0.5 + 0.2)
        rotate = Matrix44.from_z_rotation(0)

        self.use_texture.value = False

        self.light.value = (67.69, -8.14, 52.49)
        self.mvp.write((self.proj * self.lookat
            * rotate
            ).astype('f4').tobytes())

        #self.hexagon_vao.render(moderngl.TRIANGLE_FAN)

        self.color.value = (0.67, 0.49, 0.29)
        #self.objects['ground'].render()

        self.color.value = (0.46, 0.67, 0.29)
        #self.objects['grass'].render()

        self.color.value = (1.0, 1.0, 1.0)
        self.objects['billboard'].render()

        self.color.value = (0.2, 0.2, 0.2)
        self.objects['billboard-holder'].render()

        self.use_texture.value = True
        self.texture.use()

        self.objects['billboard-image'].render()

        self.color.value = (1.0, 1.0, 1.0)
        #self.use_texture.value = True

        self.pipe_textures[6].use()
        self.hexagon_vao.render(moderngl.TRIANGLE_FAN)

        hex_radius = self.hex_radius
        for i in range(6):
            angle1 = 60.*i*np.pi/180.
            angle2 = 60.*(i+1)*np.pi/180.

            translate = Matrix44.from_translation(
            (hex_radius*np.cos(angle1) + hex_radius*np.cos(angle2),
            hex_radius*np.sin(angle1) + hex_radius*np.sin(angle2), 0))

            self.mvp.write((self.proj * self.lookat
                * translate
                ).astype('f4').tobytes())

            self.pipe_textures[i].use()

            self.hexagon_vao.render(moderngl.TRIANGLE_FAN)

    @staticmethod
    def cube_round(x, y, z):
        rx = np.round(x)
        rz = np.round(z)
        ry = np.round(y)

        x_diff = np.abs(rx - x)
        y_diff = np.abs(ry - y)
        z_diff = np.abs(rz - z)

        if x_diff > y_diff and x_diff > z_diff:
            rx = -ry-rz
        elif y_diff > z_diff:
            ry = -rx-rz
        else:
            rz = -rx-ry

        return (int(rx), int(ry), int(rz))

    def mouse_press_event(self, x: int, y: int, button: int):
        width, height = self.wnd.size

        # taken from Anton Gerdelan link
        ray_clip = Vector4([(2. * x) / width - 1.0, 1.0 - (2. * y) / height, -1.0, 1.0])
        ray_eye = (~self.proj) * ray_clip
        ray_eye = Vector4([ray_eye.x, ray_eye.y, -1, 0.0])
        ray_wor = Vector3(((~self.lookat)*ray_eye).xyz)
        ray_wor.normalise()
        t = -((Vector3([0., 0., 0.]) | Vector3([0., 0., 1.])) + 50) / (ray_wor | Vector3([0., 0., 1.]))
        point = (ray_wor*t)

        # from Redblob hexagons
        size = self.hex_radius
        q = 2./3. * point.x / size
        r = -1./3. * point.x / size + np.sqrt(3.)/3. * point.y / size
        hex_key = ColorsAndTexture.cube_round(q, r, -q-r)

        # play sound
        print(hex_key)
        if hex_key in self.audio_map:
            self.audio_map[hex_key].play()
        else:
            print("No audio associated")

if __name__ == '__main__':
    run_example(ColorsAndTexture)
