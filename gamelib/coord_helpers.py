"""
Helper functions adapted from https://www.redblobgames.com/grids/hexagons/#basics

"""

import numpy as np

cube_directions = [
    (+1, -1, 0), (+1, 0, -1), (0, +1, -1),
    (-1, +1, 0), (-1, 0, +1), (0, -1, +1)
]

cardinals = np.array(cube_directions)

flat_top_matrix = np.array([
                            [3./2.,         0],
                            [np.sqrt(3.)/2., np.sqrt(3.)]
                           ])
pointy_top_matrix = np.array([
                            [ np.sqrt(3.),   np.sqrt(3.)/2.],
                            [0,             3./2.]
                            ])

def cube_add(cube1, cube2):
    return (cube1[0] + cube2[0], cube1[1] + cube2[1], cube1[2] + cube2[2])

def cube_subtract(cube1, cube2):
    return (cube1[0] - cube2[0], cube1[1] - cube2[1], cube1[2] - cube2[2])


def cube_scale(cube, scalar):
    return (cube[0] * scalar, cube[1] * scalar, cube[2] * scalar)

def cube_direction(direction):
    return cube_directions[direction]

def cube_neighbor(cube, direction):
    return cube_add(cube, cube_direction(direction))

def cube_distance(cube1, cube2):
    return max(abs(cube1[0] - cube2[0]), abs(cube1[1] - cube2[1]), abs(cube1[2] - cube2[2]))

def cube_ring(center, edge_len):
    radius = edge_len - 1
    results = []
    # this code doesn't work for radius == 0; can you see why?
    cube = cube_add(center, cube_scale(cube_direction(4), radius))
    for i in range(6):
        for j in range(radius):
            results.append(cube)
            cube = cube_neighbor(cube, i)
    return results

def cube_spiral(center, radius):
    results = [center]
    for k in range(1, radius+1):
        results.extend(cube_ring(center, k))
    return results

def cube_round(cube):
    rx = np.round(cube[0])
    ry = np.round(cube[1])
    rz = np.round(cube[2])


    x_diff = np.abs(rx - cube[0])
    y_diff = np.abs(ry - cube[1])
    z_diff = np.abs(rz - cube[2])

    if x_diff > y_diff and x_diff > z_diff:
        rx = -ry-rz
    elif y_diff > z_diff:
        ry = -rx-rz
    else:
        rz = -rx-ry

    return (int(rx), int(ry), int(rz))

def flat_hex_to_pixel(cube, size):

    return (
        size * flat_top_matrix @ cube[0:2]

        #       size * 3. / 2. * cube[0],  # x
        #       size * (np.sqrt(3.) / 2. * cube[0] + np.sqrt(3.) * cube[1])  # y
    )

def shortest_angle_between(angle1, angle2):
    if np.abs(angle1 - angle2) < 2.*np.pi - np.abs(angle1 - angle2):
        return angle1 - angle2
    else:
        return (2.*np.pi - np.abs(angle1 - angle2)) * np.sign(angle2 - angle1)

def closest_direction(vector):
    """Return the direction vector (of the 6 cardinals) closest to vector.
    Assumes vector is origin directed.
    """
    vals = np.dot(cardinals, np.asarray(vector))
    return tuple(cardinals[np.argmax(vals)])

def closest_distance(coord, coords_other):
    """Return a coordinate from coords_other which is closest to coord in cubic space.
    """
    dists = np.amax( np.abs( np.subtract( np.asarray(coords_other), np.asarray(coord))), axis = 1)
    return coords_other[np.argmin(dists)]

def hex_reachable(start, num_steps, blocked):
    visited = [start] # set of hexes
    fringes = [] # array of arrays of hexes
    fringes.append([start])

    for k in range (num_steps):
        fringes.append([])
        for cell_coord in fringes[k-1]:
            for direction in range(6):
                neighbor = cube_neighbor(cell_coord, direction)
                if neighbor not in visited and neighbor not in blocked:
                    visited.append(neighbor)
                    fringes[k].append(neighbor)
    return visited
