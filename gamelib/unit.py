
from PIL import Image
from . import data
from gamelib import coord_helpers
import numpy as np
#from gamelib import plan

class UnitType:
    UNKNOWN = 0
    FRIENDLY = 1
    ENEMY = 2

class UnitDraw:
    def __init__(self, unit, ctx):
        self.ctx = ctx

    def draw(self, use_texture):
        use_texture.value = False

class UnitTextureDraw(UnitDraw):
    def __init__(self, unit, ctx, texture_name):
        super().__init__(unit, ctx)
        self.image = Image.open(texture_name).transpose(Image.FLIP_TOP_BOTTOM).convert('RGBA')
        self.texture = self.ctx.texture(self.image.size, 4, self.image.tobytes())
        self.texture.build_mipmaps()

    def draw(self, use_texture):
        use_texture.value = True
        self.texture.use()

class Unit:
    def __init__(self, coord, direction=0, hp=0, type=UnitType.UNKNOWN):
        self.direction = direction
        self.coord = coord
        self.hp = hp
        self.type = type

    def attack(self,room): # must override
        raise NotImplementedException

    def take_damage_from(self, unit, room, damage): # must override
        raise NotImplementedException

    def is_dead(self):
        return self.hp <= 0.

    def __repr__(self):
        return str({
            "type": type(self),
            "direction": self.direction,
            "hp": self.hp
        })

class FighterDraw(UnitTextureDraw):
    def __init__(self, unit, ctx):
        super().__init__(unit, ctx, data.filepath("fighter.jpg"))

class Fighter(Unit):
    def __init__(self, coord):
        super().__init__(coord, 0, 50, type = UnitType.FRIENDLY)

    def attack(self, room):
        coords = [coord_helpers.cube_neighbor(self.coord, self.direction), # forward
            coord_helpers.cube_neighbor(self.coord, (self.direction + 1) % 6), # left
            coord_helpers.cube_neighbor(self.coord, (self.direction + 5) % 6) # right
        ]

        damages = [10., 3., 3.] # forward, left, right

        coords_damages = list(zip(coords, damages))
        coords = list(filter(lambda x: np.sum(np.abs(np.array(x[0]))) <= room.radius, coords_damages))

        for coord, damage in coords_damages:
            if coord in room.cells and not room.cells[coord].unit is None and room.cells[coord].unit != self:
                room.cells[coord].unit.take_damage_from(self, room, damage)


    def take_damage_from(self, unit, room, damage):

        # worry about direction only
        self_pixels = coord_helpers.flat_hex_to_pixel(self.coord, room.hex_size)
        other_pixels = coord_helpers.flat_hex_to_pixel(unit.coord, room.hex_size)

        direction_angle = self.direction * 60.
        angle = np.arctan2(self_pixels[1] - other_pixels[1], self_pixels[0] - other_pixels[0])

        # between 0.5 and 2.5 depending on the angle
        damage_multiplier = (np.abs(coord_helpers.shortest_angle_between(direction_angle, angle)) / np.pi)*2.0 + 0.5

        self.hp -= damage * damage_multiplier

        if self.hp <= 0.:
             # dead ...
             room.set_unit(self.coord, None)




class MonsterDraw(UnitTextureDraw):
    def __init__(self, unit, ctx):
        super().__init__(unit, ctx, data.filepath("monster.png"))

class Monster(Unit):
    def __init__(self, coord):
        super().__init__(coord, 0, 10, type=UnitType.ENEMY)

    def attack(self, room):
        coords = [coord_helpers.cube_neighbor(self.coord, self.direction), # forward
        ]

        coords = list(filter(lambda x: np.abs(x[0]) <= 5 and np.abs(x[1]) <= 5  and np.abs(x[2]) <= 5, coords))

        damages = [15.] # forward, left, right

        for coord, damage in zip(coords, damages):
            if not room.cells[coord].unit is None and room.cells[coord].unit != self:
                room.cells[coord].unit.take_damage_from(self, room, damage)

    def take_damage_from(self, unit, room, damage):

        # worry about direction only
        self_pixels = coord_helpers.flat_hex_to_pixel(self.coord, room.hex_size)
        other_pixels = coord_helpers.flat_hex_to_pixel(unit.coord, room.hex_size)

        direction_angle = self.direction * 60.
        angle = np.arctan2(self_pixels[1] - other_pixels[1], self_pixels[0] - other_pixels[0])

        # between 0.5 and 2.5 depending on the angle
        damage_multiplier = (np.abs(coord_helpers.shortest_angle_between(direction_angle, angle)) / np.pi)*2.0 + 0.5

        print("mult = ", damage_multiplier)
        print("d*m = ", damage * damage_multiplier)
        self.hp -= damage * damage_multiplier

        if self.hp <= 0.:
             # dead ...
             room.set_unit(self.coord, None)


    def generate_plan(self, room):
        """Generates a plan for the monster for attacking players.
        Return: plan
        """
