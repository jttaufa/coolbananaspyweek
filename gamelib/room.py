
from . import coord_helpers
from pyrr import Matrix44
import moderngl
import numpy as np
import json
import gamelib.unit as unit

class Cell:

    def __init__(self, coordinates, unit = None, objects = []):

        # unit in the cell
        self.unit = unit

        # cell coordinate on grid
        self.coord = coordinates

        # objects in the cell
        self.objects = objects

        # obstacles
        self.obstacle = False


class Room:

    # number of cells along one grid edge
    edge_len = 6

    # hex radius for a room
    radius = edge_len - 1

    # size of each hexagon
    hex_size = 6

    def __init__(self, obstacles = []):
        # list of hexagons in cube coordinates
        #hexagon_coords = hexagon.cube_spiral((0, 0, 0), Room.radius)
        cell_coords = coord_helpers.cube_spiral((0,0,0), Room.edge_len)

        self.obstacle_coords = []

        # construct room hexagons
        self.cells = {}
        self.cell_to_coord = {}
        for cell_coord in cell_coords:

            cell = Cell(cell_coord)

            if cell_coord in self.obstacle_coords:
                cell.obstacle = True

            # store room hexagon by coordinate
            self.cells[cell_coord] = cell

            # reverse lookup
            self.cell_to_coord[cell] = cell_coord

        self.print_once = True

    def set_unit(self, coord, unit):
        self.cells[coord].unit = unit

    def remove_unit(self, coord):
        self.cells[coord].unit = None

    def add_object(self, coord, obj):
        self.cells[coord].objects.append(obj)

    def remove_object(self, coord, obj):
        self.cells[coord].objects.remove(obj)

    def set_obstacle(self, coord):
        self.cells[coord].obstacle = True
        self.obstacle_coords.append(coord)

    def get_obstacles(self):
        return self.obstacles


    def draw(self, hexagon_vao, mvp, prog_mvp, use_texture, color):
        for coord, room in self.cells.items():

            # this is actually in (m) ... world coordinates
            #pixel = hexagon.flat_hex_to_pixel(coord, Room.hex_size)
            pixel = coord_helpers.flat_hex_to_pixel(np.array(coord), Room.hex_size)

            if self.print_once:
                print(coord, " -- > ", pixel)

            translate = Matrix44.from_translation((
                pixel[0], pixel[1], 0))

            prog_mvp.write((mvp * translate).astype('f4').tobytes())

            if self.cells[coord].obstacle == True: #Set obstacles as black
                color.value = (0, 0, 0)
                use_texture.value = False
            else:
                color.value = (1,1,1)
                if not room.unit is None:
                    room.unit.draw.draw(use_texture)
                elif len(room.objects) > 0:
                    # do something interesting
                    pass
                else:
                    use_texture.value = False


            hexagon_vao.render(moderngl.TRIANGLE_FAN)

        self.print_once = False


class RoomLoader:
    def __init__(self, filename, room):
        with open(filename, "r") as f:
            json_room = json.loads(f.read())

        self.room = room
        self.units = []

        json_cells = json_room["cells"]

        for coord, json_cell in json_cells:

            try:
                unit_type = json_cells["unit_type"]
            except KeyError as ke:
                unit_type = None

            if unit_type != "None":
                unit = self._create_unit(unit_type, coord)
                room.set_unit(coord, unit)

            try:
                json_objects = json_cells["objects"]
            except KeyError as ke:
                json_objects = []

            for json_obj in json_objects:
                object_type = json_obj["object_type"]
                obj = self._create_object(object_type)
                room.add_object(coord, obj)

    def _create_unit(self, unit_type, coord):
        if unit_type.endswith("Monster"):
            return unit.Monster(coord)

    def get_room(self):
        return self.room

    def get_units(self):
        return self.units


class RoomSaver:
    def __init__(self, filename, room):
        self.room = room

        json_room = {}
        json_cells = {}
        json_room["cells"] = json_cells

        for coord, cell in self.room.cells.items():

            json_cell = {}
            json_cells[coord] = json_cell

            if cell.unit is None:
                json_cell["unit_type"] = "None"
            else:
                json_cell["unit_type"] = type(cell.unit)

            json_objects = []
            json_cell["objects"] = json_objects
            for obj in cell.objects:
                json_object = {}
                json_objects.append(json_object)

                json_object["object_type"] = type(obj)

        with open(filename, "w") as f:
            f.write(json.dumps(json_room))
