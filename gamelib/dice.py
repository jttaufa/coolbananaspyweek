from . import coord_helpers
from pyrr import Matrix44
import moderngl
import numpy as np
from gamelib.room import Room

from PyQt5.QtCore import QSize
from PyQt5.QtGui import QImage, QPainter, QColor, QFont

import random

class Dice6:

    def __init__(self, coord):
        self.number = -1
        self.coord = coord
        self.unit = None

    def roll(self):
        self.number = random.randint(0, 5)

    def set_unit(self, unit):
        self.unit = unit

class Dice6d6:

    def __init__(self, ctx):
        coords = coord_helpers.cube_spiral((0, 0, 0), 2)
        coords.remove((0, 0, 0))

        # construct room hexagons
        self.dice = {}
        self.dice_to_coord = {}

        for coord in coords:

            dice6 = Dice6(coord)

            # store room hexagon by coordinate
            self.dice[coord] = dice6

            # reverse lookup
            self.dice_to_coord[dice6] = coord


        self.dice_textures = []

        for i in range(6):
            q_image = QImage(QSize(50, 50), QImage.Format_ARGB32)
            q_image.fill(QColor(255, 200, 200, 255))

            font = QFont()
            font.setPointSize(25)

            painter = QPainter()
            painter.begin(q_image)
            painter.setPen(QColor(0, 0, 100, 255))
            painter.setFont(font)
            painter.drawText(10, 10, 50, 50, 0,
              "%i" % (i+1))
            painter.end()

            ptr = q_image.mirrored().bits()
            ptr.setsize(q_image.byteCount())

            q_tex = ctx.texture((50, 50), 4, ptr.asstring())

            self.dice_textures.append(q_tex)

        self.done = False


    def roll(self):
        self.done = False
        for coord, dice in self.dice.items():
            dice.roll()
            yield

        self.done = True

    def is_done(self):
        return self.done

    def clear(self):
        for coord, dice in self.dice.items():
            dice.number = -1
            dice.set_unit(None)

    def draw(self, hexagon_vao, mvp, prog_mvp, use_texture, color, time):
        for coord, dice in self.dice.items():

            # this is actually in (m) ... world coordinates
            #pixel = hexagon.flat_hex_to_pixel(coord, Room.hex_size)
            pixel = coord_helpers.flat_hex_to_pixel(np.array(coord), Room.hex_size)

            #if self.print_once:
            #    print(coord, " -- > ", pixel)

            translate = Matrix44.from_translation((
                pixel[0], pixel[1], 0))

            prog_mvp.write((mvp * translate).astype('f4').tobytes())

            color.value = (1.0, 1.0, 1.0)

            if dice.number >= 0:
                use_texture.value = True
                self.dice_textures[dice.number].use()
            else:
                use_texture.value = False

            hexagon_vao.render(moderngl.TRIANGLE_FAN)


    def draw_overlay(self, hexagon_vao, mvp, prog_mvp, use_texture, color, time):

        for coord, dice in self.dice.items():

            if dice.unit is None:
                continue

            # this is actually in (m) ... world coordinates
            #pixel = hexagon.flat_hex_to_pixel(coord, Room.hex_size)
            pixel = coord_helpers.flat_hex_to_pixel(dice.unit.coord, Room.hex_size)

            scale = Matrix44.from_scale((0.5, 0.5, 0.5))

            translate = Matrix44.from_translation((
                pixel[0]+Room.hex_size/2, pixel[1]+Room.hex_size/2, 1))

            prog_mvp.write((mvp * translate * scale).astype('f4').tobytes())
            color.value = (1.0, 1.0, 1.0)

            use_texture.value = True
            self.dice_textures[dice.number].use()

            hexagon_vao.render(moderngl.TRIANGLE_FAN)
