
import gamelib.coord_helpers as coord_helpers
from pyrr import Matrix44
import moderngl
import numpy as np
import json
from gamelib.room import Room
from gamelib.unit import UnitType

action_costs = {
    "NOP": 1,

    # orient in 6 directions
    "ORIENT_0": 0,
    "ORIENT_1": 0,
    "ORIENT_2": 0,
    "ORIENT_3": 0,
    "ORIENT_4": 0,
    "ORIENT_5": 0,

    # move in the oriented direction
    "MOVE": 1,

    # attack in the oriented direction
    "ATTACK": 1,

    # push?
    "PUSH": 1,

}

action_map = {
    0: "NOP",
    10: "ORIENT_0",
    11: "ORIENT_1",
    12: "ORIENT_2",
    13: "ORIENT_3",
    14: "ORIENT_4",
    15: "ORIENT_5",
    20: "MOVE",
    30: "ATTACK",
    40: "PUSH"
}

reverse_action_map = {
    "NOP": 0,
    "ORIENT_0": 10,
    "ORIENT_1": 11,
    "ORIENT_2": 12,
    "ORIENT_3": 13,
    "ORIENT_4": 14,
    "ORIENT_5": 15,
    "MOVE": 20,
    "ATTACK": 30,
    "PUSH": 40
}

# NOP = 0
#
# # orient in 6 directions
# ORIENT_0 = 10
# ORIENT_1 = 11
# ORIENT_2 = 12
# ORIENT_3 = 13
# ORIENT_4 = 14
# ORIENT_5 = 15
#
# # move in the oriented direction
# MOVE = 20
#
# # attack in the oriented direction
# ATTACK = 30
#
# # push?
# PUSH = 40


class Action:

    NOP = 0

    # orient in 6 directions
    ORIENT_0 = 10
    ORIENT_1 = 11
    ORIENT_2 = 12
    ORIENT_3 = 13
    ORIENT_4 = 14
    ORIENT_5 = 15

    # move in the oriented direction
    MOVE = 20

    # attack in the oriented direction
    ATTACK = 30

    # push?
    PUSH = 40

    orient = {
        (+1, -1, 0): ORIENT_0,
        (+1, 0, -1): ORIENT_1,
        (0, +1, -1): ORIENT_2,
        (-1, +1, 0): ORIENT_3,
        (-1, 0, +1): ORIENT_4,
        (0, -1, +1): ORIENT_5
    }

    reverse_orient = {
        ORIENT_0: (+1, -1, 0),
        ORIENT_1: (+1, 0, -1),
        ORIENT_2: (0, +1, -1),
        ORIENT_3: (-1, +1, 0),
        ORIENT_4: (-1, 0, +1),
        ORIENT_5: (0, -1, +1)
    }

    def __init__(self, action_type):
        self.action_type = action_type
        self.cost = action_costs[action_map[action_type]]

    def __repr__(self):
        return str(action_map[self.action_type])

orient_actions = {
    Action.ORIENT_0: 0,
    Action.ORIENT_1: 1,
    Action.ORIENT_2: 2,
    Action.ORIENT_3: 3,
    Action.ORIENT_4: 4,
    Action.ORIENT_5: 5
}

orient_actions_reverse = {
    0: Action.ORIENT_0,
    1: Action.ORIENT_1,
    2: Action.ORIENT_2,
    3: Action.ORIENT_3,
    4: Action.ORIENT_4,
    5: Action.ORIENT_5
}


class UnitPlan:
    def __init__(self, unit, initial_coord, num_moves):
        self.unit = unit

        # the cube coordinate of the unit
        self.initial_coord = initial_coord

        # num_moves taken from a dice roll
        self.num_moves = num_moves

        # list of actions to perform
        self.actions = []

        self.plan_cost = 0

        # for optimising actions
        self.orientation_actions = []


    def add_action(self, action):
        if self.plan_cost + action.cost <= self.num_moves:

            if action.action_type in Action.reverse_orient:

                # two orientations in a row are pointless, so remove old one
                if len(self.actions) > 0 and self.actions[-1].action_type in Action.reverse_orient:
                    old_action = self.actions.pop(-1)
                    self.plan_cost -= old_action.cost
                    self.orientation_actions.pop(-1)

                # filter out multiple orientations that are the same direction
                if len(self.orientation_actions) > 0 and action.action_type == self.orientation_actions[-1].action_type:
                    print("Same")
                    return

                self.orientation_actions.append(action)
                print(self.orientation_actions)

            self.actions.append(action)
            self.plan_cost += action.cost

    def get_actions(self):
        return self.actions

    def remove_action(self):
        if len(self.actions) > 0:
            action = self.actions.pop(-1)
            self.plan_cost -= action.cost

            if action.action_type in Action.reverse_orient:
                self.orientation_actions.pop(-1)

    def __repr__(self):
        return str({
            "Num_moves": self.num_moves,
            "Actions": self.actions
        })

class Plan:

    def __init__(self):
        self.unit_plans = {}

    def add_unit_plan(self, plan):
        self.unit_plans[plan.unit] = plan

    def get_unit_plan(self, unit):
        return self.unit_plans[unit]

    def contains_unit_plan_for(self, unit):
        return unit in self.unit_plans

    def get_unit_plan_via_room(self, room, coord, **kwargs):
        unit = room.cells[coord].unit
        if unit is None:
            return None
        else:
            unit_plan = self.unit_plans[unit]

            if not unit_plan is None:
                return unit_plan
            elif kwargs.get("create", False):
                UnitPlan(unit, coord, num_moves)

    @staticmethod
    def generate_enemy_plan(room, units, dice_rolls):
        plan = Plan()

        #Get list of friendlies
        friendly_units = []
        for unit in units:
            if (not unit.is_dead()) and (unit.type == UnitType.FRIENDLY):
                friendly_units.append(unit)


        min_dist = np.inf
        for unit in units:
            if (not unit.is_dead()) and (unit.type == UnitType.ENEMY): #Valid enemy
                num_moves = dice_rolls.pop(0) #Set number steps

                unit_plan = UnitPlan(unit, unit.coord, num_moves)

                #Attempt to map path to friendly
                action = Action(Action.NOP) #default - waste one move if not overriden
                last_orient = Action.ORIENT_0
                last_coord = unit.coord
                while unit_plan.plan_cost < num_moves:
                    # Find closest friendly character
                    nearest_friendly_coord = coord_helpers.closest_distance(last_coord, [friendly.coord for friendly in friendly_units])

                    # Find orientation to closest friendly
                    friendly_vector = coord_helpers.cube_subtract(nearest_friendly_coord, last_coord)
                    orient = Action.orient[coord_helpers.closest_direction(friendly_vector)]

                    print("Friendly vector: ",friendly_vector, "Direction: ",orient)

                    if (orient != last_orient):
                        last_orient = orient
                        # Rotate toward nearest friendly unit (costs 0)
                        unit_plan.add_action(Action(orient))

                    if (coord_helpers.cube_distance(unit.coord, nearest_friendly_coord) < 2): #Neighbour - attack
                        action = Action(Action.ATTACK)
                    else:
                        next_coord = coord_helpers.cube_add(last_coord, Action.reverse_orient[last_orient])
                        if (room.cells[next_coord].obstacle): #Hit obstacle - do nothing
                            action = Action(Action.NOP)
                        else:
                            action = Action(Action.MOVE) #Approach
                        last_coord = coord_helpers.cube_add(last_coord, Action.reverse_orient[orient])
                    unit_plan.add_action(action)

                plan.add_unit_plan(unit_plan)


                #coord_helpers.hex_reachable(unit.coord, num_moves, room.obstacle_coords)

        return plan




class UnitPlanDraw:

    color_map = {
        0: (1., 0., 0.),
        1: (0., 1., 0.),
        2: (0., 0., 1.),
        3: (0.5, 0.5, 0.),
        4: (0., 0.5, 0.5),
        5: (0.5, 0., 0.5),
    }

    def __init__(self, unit_plan, unit_id, room, ctx, prog):
        self.unit_plan = unit_plan
        self.room = room
        self.ctx = ctx
        self.unit_id = unit_id

        tri_verts = []
        tri_verts.append([0, 0, 0, 0, 0, 1, 0.5, 0.5])

        radius = Room.radius

        for i in range(2):
            angle = 60. * i # in degrees

            tri_verts.append([radius*np.cos(angle * np.pi / 180.0), radius*np.sin(angle * np.pi / 180.0), 0,
                0, 0, 1, # normals
                (np.cos(angle * np.pi / 180.0)+1.)/2., (np.sin(angle * np.pi / 180.0) + 1.)/2. # tex coords
            ])

        self.tri_vbo = self.ctx.buffer(np.array(tri_verts, dtype="f4"))
        self.tri_vao = self.ctx.simple_vertex_array(prog, self.tri_vbo, 'in_vert', 'in_norm', 'in_text')

        self.line_vbo = self.ctx.buffer(reserve=1024, dynamic=True)
        self.line_vao = self.ctx.simple_vertex_array(prog, self.line_vbo, 'in_vert', 'in_norm', 'in_text')

    def draw(self, hexagon_vao, mvp, prog_mvp, color):
        # this is actually in (m) ... world coordinates
        current_coord = self.unit_plan.initial_coord

        current_direction = self.unit_plan.unit.direction

        orient_actions = {
            Action.ORIENT_0: 0,
            Action.ORIENT_1: 1,
            Action.ORIENT_2: 2,
            Action.ORIENT_3: 3,
            Action.ORIENT_4: 4,
            Action.ORIENT_5: 5
        }


        vertices = []

        pixel = coord_helpers.flat_hex_to_pixel(current_coord, Room.hex_size)
        vertices.extend((pixel[0], pixel[1], 1))
        vertices.extend((0., 0., 1.))
        vertices.extend((0., 0))

        for i, action in enumerate(self.unit_plan.actions):

            if action.action_type in orient_actions:

                # coord stays the same
                current_direction = orient_actions[action.action_type]

            elif action.action_type == Action.MOVE:

                current_coord = coord_helpers.cube_neighbor(current_coord, current_direction)



            pixel = coord_helpers.flat_hex_to_pixel(current_coord, Room.hex_size)

            translate = Matrix44.from_translation((
                pixel[0], pixel[1], 1))

            rotate = Matrix44.from_z_rotation(-np.pi/3. * (current_direction-1))

            prog_mvp.write((mvp * translate * rotate).astype('f4').tobytes())

            if action.action_type == Action.MOVE:
                vertices.extend((pixel[0], pixel[1], 1))
                vertices.extend((0., 0., 1.))
                vertices.extend((0., 0))
                vertices.extend((pixel[0], pixel[1], 1))
                vertices.extend((0., 0., 1.))
                vertices.extend((0., 0))

            color.value = UnitPlanDraw.color_map[self.unit_id]

            if action.action_type in orient_actions or i == len(self.unit_plan.actions)-1:
                #hexagon_vao.render(moderngl.TRIANGLE_FAN)
                self.tri_vao.render(moderngl.TRIANGLE_FAN)

        pixel = coord_helpers.flat_hex_to_pixel(current_coord, Room.hex_size)
        vertices.extend((pixel[0], pixel[1], 1))
        vertices.extend((0., 0., 1.))
        vertices.extend((0., 0))

        prog_mvp.write((mvp).astype('f4').tobytes())

        color.value = UnitPlanDraw.color_map[self.unit_id]
        self.line_vbo.clear()
        self.line_vbo.write(np.array(vertices, dtype="f4"))
        self.line_vao.render(moderngl.LINES)

class PlanLoader:

    def __init__(self, filename, room):
        with open(filename, "r") as f:
            json_plan = json.loads(f.read())



        self.plan_friendly = Plan()
        self.plan_enemy = Plan()

        self._load_plan(self.plan_friendly, json_plan["unit_plans_friendly"], room)
        self._load_plan(self.plan_enemy, json_plan["unit_plans_enemy"], room)

    def _load_plan(self, plan, unit_plans_json, room):
        for unit_plan_json in unit_plans_json:

            initial_coord = tuple(unit_plan_json["initial_coord"])
            print(initial_coord)
            unit = room.cells[initial_coord].unit
            print(unit)

            num_moves = unit_plan_json["num_moves"]

            unit_plan = UnitPlan(unit, initial_coord, num_moves)


            actions_json = unit_plan_json["actions"]

            for action_json in actions_json:

                action_type_str = action_json["action_type"]

                action_type = getattr(Action, action_type_str)

                unit_plan.add_action(Action(action_type))

            plan.add_unit_plan(unit_plan)

    def get_plan_friendly(self):
        return self.plan_friendly

    def get_plan_enemy(self):
        return self.plan_enemy
