import gamelib.plan as plan
from gamelib.plan import Action
import gamelib.coord_helpers as coord_helpers
import numpy as np

class PlanExecutor():
    def __init__(self, room, plan_players, plan_enemies):
        self.plan_players = plan_players
        self.plan_enemies = plan_enemies
        self.room = room
        self.sequence = []

        self.i_sequence = 0
        self.i_action = 0

        self.sequence_actions()

        self.done = False


    def sequence_actions(self):
        #Concatenate enemies and players plans
        full_plan = list(self.plan_enemies.unit_plans.values()) + list(self.plan_players.unit_plans.values())

        self.sequence = sorted(full_plan, key = lambda unit_plan: unit_plan.num_moves)

        self.i_sequence = 0


        #print(self.sequence)
        #raise SystemExit

    def step(self):

        if (self.i_sequence >= len(self.sequence)):
            self.done = True
            return False

        current_plan = self.sequence[self.i_sequence]

        if len(current_plan.get_actions()) == 0:
            self.i_sequence += 1
            return True

        current_action = current_plan.get_actions()[self.i_action]

        print(current_plan)
        print(current_action)
        print(self.i_action)
        print(self.i_sequence)



        #Execute action
        if not self.execute(current_plan, current_action):
            # unit has died, so abandon plan
            self.i_action = len(current_plan.get_actions())
        else:
            self.i_action += 1



        if (self.i_action >= len(current_plan.get_actions())):
            self.i_sequence += 1
            self.i_action = 0
            if (self.i_sequence >= len(self.sequence)):
                #Completed all actions!
                self.done = True
                return False
        return True

    def execute(self, unit_plan, action): # return false if unit has died
        unit = unit_plan.unit

        if unit.is_dead():
            return False

        cell = self.room.cells[unit.coord]

        if (action.action_type in plan.orient_actions):
            unit.direction = plan.orient_actions[action.action_type]
            return True

        elif (action.action_type == Action.MOVE):
            new_coord = coord_helpers.cube_neighbor(unit.coord, unit.direction)

            # don't allow going off the end of room
            if np.abs(new_coord[0]) > 5 or np.abs(new_coord[1]) > 5 or np.abs(new_coord[2]) > 5:
                return True

            # don't allow units to stack
            if self.room.cells[new_coord].unit != None:
                # push?

                return True


            unit.coord = new_coord
            cell.unit = None
            self.room.set_unit(unit.coord, unit)

            #self.room.cells[unit.coord].unit = unit

            return True

        elif action.action_type == Action.ATTACK:

            unit.attack(self.room)

            return True

    def is_done(self):
        return self.done
