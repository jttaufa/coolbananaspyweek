from . import coord_helpers
from pyrr import Matrix44
import moderngl
import numpy as np
from gamelib.room import Room

class OverviewRoom: # a room in the overview

    def __init__(self, coord):
        self.explored = False
        self.coord = coord
        self.selected = False


class Map:

    # hex radius for a room
    radius = 5

    # size of each hexagon
    hex_size = 6

    def __init__(self):
        room_coords = []

        origin = (0, 5, -5)
        self.selected = coord_helpers.cube_neighbor(origin, 2)
        self.current = None

        for i in range(4):
            room_coords.extend(coord_helpers.cube_spiral(origin, 2))
            origin = coord_helpers.cube_neighbor(origin, 5) # downwards
            origin = coord_helpers.cube_neighbor(origin, 5) # downwards
            origin = coord_helpers.cube_neighbor(origin, 5) # downwards

        # construct room hexagons
        self.rooms = {}
        self.room_to_coord = {}

        for room_coord in room_coords:

            if room_coord in self.rooms:
                continue

            room = OverviewRoom(room_coord)

            # store room hexagon by coordinate
            self.rooms[room_coord] = room

            # reverse lookup
            self.room_to_coord[room] = room_coord

        self.last_change_time = 0.
        self.change = False

        #self.enter_selected()
        #self.current_completed()

    def select(self, coord):
        if self.current is None:
            return # do nothing

        else:
            neighbors = [coord_helpers.cube_neighbor(self.current, i) for i in range(6)]
            neighbors = filter(lambda x: x in self.rooms, neighbors)

            if coord in neighbors:
                self.selected = coord

    def enter_selected(self):
        self.current = self.selected
        self.selected = None

    def current_completed(self):
        self.rooms[self.current].explored = True

    def draw(self, hexagon_vao, mvp, prog_mvp, use_texture, color, time):
        for coord, room in self.rooms.items():

            # this is actually in (m) ... world coordinates
            #pixel = hexagon.flat_hex_to_pixel(coord, Room.hex_size)
            pixel = coord_helpers.flat_hex_to_pixel(np.array(coord), Room.hex_size)

            #if self.print_once:
            #    print(coord, " -- > ", pixel)

            translate = Matrix44.from_translation((
                pixel[0], pixel[1], 0))

            prog_mvp.write((mvp * translate).astype('f4').tobytes())

            use_texture.value = False

            if room.explored:
                color.value = (0.4, 1., 0.4)
            else:
                color.value = (1.0, 0.4, 0.4)


            if self.selected == coord:
                if time - self.last_change_time > 0.2:
                    self.change = not self.change
                    self.last_change_time = time

                if self.change:
                    color.value = (color.value[0]*0.5, color.value[1]*0.5, color.value[2] * 0.5)

            elif self.current == coord:
                color.value = (0.2, 0.2, 0.2)

            hexagon_vao.render(moderngl.TRIANGLE_FAN)

        #self.print_once = False
