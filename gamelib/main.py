'''
The main game class, built on moderngl example windows for now
'''

import os
import sys
import moderngl
import numpy as np
from objloader import Obj
from PIL import Image
from pyrr import Matrix44, Vector4, Vector3
from PyQt5.QtMultimedia import QSound
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QImage, QPainter, QColor, QFont
from . import data
from .window import Example, run_example

from gamelib.unit import *
from gamelib.plan import *
from gamelib.room import Room, Cell
from gamelib.plan_executor import PlanExecutor
from gamelib.overview import OverviewRoom, Map
from gamelib.dice import Dice6, Dice6d6

import gamelib.coord_helpers as coord_helpers
import random

class Main(Example):
    gl_version = (3, 3)
    title = "66666666666666666666666666"

    OVERVIEW = 5
    ROLLING = 9
    PLANNING = 10
    EXECUTING = 20
    ROOM_FINISHED = 30
    GAME_OVER = 40

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.prog = self.ctx.program(
            vertex_shader='''
                #version 330

                uniform mat4 Mvp;

                in vec3 in_vert;
                in vec3 in_norm;
                in vec2 in_text;

                out vec3 v_vert;
                out vec3 v_norm;
                out vec2 v_text;

                void main() {
                    gl_Position = Mvp * vec4(in_vert, 1.0);
                    v_vert = in_vert;
                    v_norm = in_norm;
                    v_text = in_text;
                }
            ''',
            fragment_shader='''
                #version 330

                uniform vec3 Light;
                uniform vec3 Color;
                uniform bool UseTexture;
                uniform sampler2D Texture;

                in vec3 v_vert;
                in vec3 v_norm;
                in vec2 v_text;

                out vec4 f_color;

                void main() {
                    float lum = clamp(dot(normalize(Light - v_vert), normalize(v_norm)), 0.0, 1.0) * 0.8 + 0.2;
                    if (UseTexture) {
                        f_color = vec4(texture(Texture, v_text).rgb * lum, texture(Texture, v_text).a);
                    } else {
                        f_color = vec4(Color * lum, 1.0);
                    }
                }
            ''',
        )

        self.mvp = self.prog['Mvp']
        self.light = self.prog['Light']
        self.color = self.prog['Color']
        self.use_texture = self.prog['UseTexture']

        self.objects = {}

        for name in ['scene-1-ground', 'scene-1-grass', 'scene-1-billboard', 'scene-1-billboard-holder', 'scene-1-billboard-image']:

            obj = Obj.open(data.filepath('%s.obj' % name))
            vbo = self.ctx.buffer(obj.pack('vx vy vz nx ny nz tx ty'))
            vao = self.ctx.simple_vertex_array(self.prog, vbo, 'in_vert', 'in_norm', 'in_text')
            if name.startswith("scene-1-"):
                self.objects[name[len("scene-1-"):]] = vao
            else:
                self.objects["billboard-image"] = vao


        info_img = Image.open(data.filepath('infographic-1.jpg')).transpose(Image.FLIP_TOP_BOTTOM).convert('RGB')
        self.texture = self.ctx.texture(info_img.size, 3, info_img.tobytes())
        self.texture.build_mipmaps()

        self.pipe_textures = {}

        for i, pipe_no in enumerate(["one", "two", "three", "four", "five", "six", "seven"]):
            try:
                img = Image.open(data.filepath('%s.png' % pipe_no)).transpose(Image.FLIP_TOP_BOTTOM).convert('RGB')
            except:
                img = info_img

            self.pipe_textures[i] = self.ctx.texture(img.size, 3, img.tobytes())
            self.pipe_textures[i].build_mipmaps()

        hexagon_verts = []
        self.hex_radius = Room.hex_size
        radius = self.hex_radius
        # centre point
        hexagon_verts.append([0, 0, 0, 0, 0, 1, 0.5, 0.5])

        for i in range(7):
            angle = 60. * i # in degrees

            hexagon_verts.append([radius*np.cos(angle * np.pi / 180.0), radius*np.sin(angle * np.pi / 180.0), 0,
                0, 0, 1, # normals
                (np.cos(angle * np.pi / 180.0)+1.)/2., (np.sin(angle * np.pi / 180.0) + 1.)/2. # tex coords
            ])

        self.hexagon_vbo = self.ctx.buffer(np.array(hexagon_verts, dtype="f4"))
        self.hexagon_vao = self.ctx.simple_vertex_array(self.prog, self.hexagon_vbo, 'in_vert', 'in_norm', 'in_text')

        # now for some audio
        # maps hex cube coordinates to waves to play
        # (0, 0, 0) is centre hex
        self.audio_map = {}
        audio_files = ["one", "two", "three"]
        cube_coordinates = [(0, 0, 0), (1, 0, -1), (0, 1, -1)]

        for audio_file, cube_coord in zip(audio_files, cube_coordinates):
            filename = data.filepath("%s.wav" % audio_file)
            if not os.path.exists(filename):
                filename = data.filepath("one.wav")
            self.audio_map[cube_coord] = QSound(filename)

        self.q_image = QImage(QSize(800, 40), QImage.Format_ARGB32)
        self.q_image.fill(QColor(255, 255, 255, 0))

        font = QFont()
        font.setPointSize(30)

        painter = QPainter()
        painter.begin(self.q_image)
        painter.setPen(QColor(0, 0, 100, 255))
        painter.setFont(font)
        painter.drawText(0, 0, 800, 40, 0,
          "Connect the wizard pipe to the dank weed.")
        painter.end()

        ptr = self.q_image.bits()
        ptr.setsize(self.q_image.byteCount())

        self.q_tex = self.ctx.texture((800, 40), 4, ptr.asstring())

        text_verts = np.array([
            [0., 0., 0., 0., 0., 1., 0., 1.],
            [80., 0., 0., 0., 0., 1., 1., 1.],
            [0., 4., 0., 0., 0., 1., 0., 0.],
            [80., 4., 0., 0., 0., 1., 1., 0.],
        ], dtype="f4")

        self.text_vbo = self.ctx.buffer(text_verts)
        self.text_vao = self.ctx.simple_vertex_array(self.prog, self.text_vbo, 'in_vert', 'in_norm', 'in_text')

        self.scale_i = 100.0



        #central_fighter = Fighter((0, 0, 0))
        #central_fighter.draw = FighterDraw(central_fighter, self.ctx)
        #self.room.set_unit((0, 0, 0), central_fighter)

        self.plan_friendly = Plan()
        self.plan_enemy = Plan()
        #
        # self.central_fighter_plan = UnitPlan(central_fighter, (0, 0, 0), 5)
        # self.central_fighter_plan.draw = UnitPlanDraw(self.central_fighter_plan, 0,  self.room, self.ctx, self.prog)
        #
        # self.central_fighter_plan.add_action(Action(Action.ORIENT_0))
        # self.central_fighter_plan.add_action(Action(Action.MOVE))
        # self.central_fighter_plan.add_action(Action(Action.MOVE))
        # self.central_fighter_plan.add_action(Action(Action.MOVE))
        # self.central_fighter_plan.add_action(Action(Action.MOVE))
        # self.central_fighter_plan.add_action(Action(Action.ATTACK))
        #
        # self.plan.add_unit_plan(self.central_fighter_plan)
        #
        #
        # central_fighter2 = Fighter((1, 0, -1))
        # central_fighter2.draw = FighterDraw(central_fighter2, self.ctx)
        # self.room.set_unit((1, 0, -1), central_fighter)
        #
        # self.central_fighter_plan2 = UnitPlan(central_fighter2, (1, 0, -1), 6)
        # self.central_fighter_plan2.draw = UnitPlanDraw(self.central_fighter_plan2, 1,  self.room, self.ctx, self.prog)
        #
        # self.central_fighter_plan2.add_action(Action(Action.ORIENT_0))
        # self.central_fighter_plan2.add_action(Action(Action.MOVE))
        # self.central_fighter_plan2.add_action(Action(Action.MOVE))
        # self.central_fighter_plan2.add_action(Action(Action.ORIENT_5))
        # self.central_fighter_plan2.add_action(Action(Action.MOVE))
        # self.central_fighter_plan2.add_action(Action(Action.ATTACK))
        # self.central_fighter_plan2.add_action(Action(Action.MOVE))
        # self.central_fighter_plan2.add_action(Action(Action.MOVE))

        self.renderer_counter = 0

        self.plans = ["plan1", "plan2", "plan3", "plan4"]
        self.current_plan = 0
        #self.load_next_plan()

        self.paused = False
        self.rate = 10

        # initial mode
        self.mode = Main.OVERVIEW


        self.overview = Map()
        self.dice = Dice6d6(self.ctx)
        self.dice_roller = None


        self.dragging = False

        # self.mode = Main.ROLLING
        self.dice_roller = self.dice.roll()
        self.dice_center = (-6, 8, -2)

        self.units_friendly = []

        def make_fighter(coord):
            fighter = Fighter(coord)
            fighter.draw = FighterDraw(fighter, self.ctx)
            self.units_friendly.append(fighter)

        friendly_coords = [(-5, 0, 5),
                           (-5, 1, 4),
                           (-5, 2, 3),
                           (-5, 3, 2),
                           (-5, 4, 1),
                           (-5, 5, 0)]
        for coord in friendly_coords:
            make_fighter(coord)


    def generate_room(self):
        self.room = Room()

        self.units = []+self.units_friendly

        friendly_coords = [(-5, 0, 5),
                           (-5, 1, 4),
                           (-5, 2, 3),
                           (-5, 3, 2),
                           (-5, 4, 1),
                           (-5, 5, 0)]

        i = 0
        for unit in self.units:
            if not unit.is_dead():
                self.room.set_unit(friendly_coords[i], unit)
                unit.coord = friendly_coords[i]
                i += 1

        #Make obstacles
        def make_obstacle(coord):
            self.room.set_obstacle(coord)

        num_obstacles = random.randint(1, 6) #0 to 6 obstacles
        obstacle_coords = []
        self.room.edge_len - 2
        obstacle_range_x = [-(self.room.edge_len-2), self.room.edge_len-2]
        print("Obstacles: ",num_obstacles)
        for i_obstacle in range(num_obstacles):
            #Generate coordinates for obstacle
            valid_coord = False
            while (not valid_coord):
                # Generate coordinates for monster
                x = random.randint(obstacle_range_x[0], obstacle_range_x[1])
                y = random.randint(min(0, -x), max(0, -x))
                z = -(x + y)
                coord = (x, y, z)

                if (coord in self.room.cells.keys()) and \
                        coord not in obstacle_coords and \
                        coord not in friendly_coords:
                    obstacle_coords.append(coord)
                    valid_coord = True
                    make_obstacle(coord)
                    print(obstacle_coords)


        #Make monsters
        def make_monster(coord):
            monster = Monster(coord)
            monster.draw = MonsterDraw(monster, self.ctx)
            self.room.set_unit(coord, monster)
            self.units.append(monster)

        num_monsters = random.randint(1, 6) #1 to 6 monsters
        print("Monsters: ",num_monsters)
        monster_coords = []
        monster_range_x = [1, self.room.edge_len-1]
        for i_monster in range(num_monsters):
            valid_coord = False
            while (not valid_coord):
                #Generate coordinates for monster
                x = random.randint(monster_range_x[0],monster_range_x[1])
                y = random.randint(min(0, -x), max(0, -x))
                z = -(x+y)
                coord = (x,y,z)

                if (coord in self.room.cells.keys()) and \
                        coord not in monster_coords and \
                        coord not in obstacle_coords and \
                        coord not in friendly_coords:
                    make_monster(coord)
                    monster_coords.append(coord)
                    valid_coord = True

        """for coord in [(5, 0, -5),
                      (5, -1, -4),
                      (5, -2, -3),
                      (5, -3, -2),
                      (5, -4, -1),
                      (5, -5, 0)]:
            make_monster(coord)"""

    def load_plan(self, plan_file):

        self.plan_loader = PlanLoader(data.filepath("%s.json" % plan_file), self.room)

        i = 0
        for unit, unit_plan in self.plan_loader.get_plan_friendly().unit_plans.items():
            unit_plan.draw = UnitPlanDraw(unit_plan, i,  self.room, self.ctx, self.prog)
            i += 1

        i = 0
        for unit, unit_plan in self.plan_loader.get_plan_enemy().unit_plans.items():
            unit_plan.draw = UnitPlanDraw(unit_plan, i,  self.room, self.ctx, self.prog)
            i += 1

        self.plan_executor = PlanExecutor(self.room, self.plan_loader.get_plan_friendly(), self.plan_loader.get_plan_enemy())

    def add_plan_draw(self, plan):
        i = 0
        for unit, unit_plan in plan.unit_plans.items():
            unit_plan.draw = UnitPlanDraw(unit_plan, i,  self.room, self.ctx, self.prog)
            i += 1


    def load_next_plan(self):
        if self.current_plan < len(self.plans):
            self.load_plan(self.plans[self.current_plan])
            self.current_plan += 1

    def render(self, time: float, frame_time: float):

        # updates
        if self.mode == Main.EXECUTING:
            if ((self.renderer_counter % self.rate) == 0 and not self.paused):
                if self.plan_executor.step():
                    print("Stepped!")

                friendly_count = 0
                enemy_count = 0
                for unit in self.units:
                    if unit.is_dead():
                        continue

                    if unit.type == UnitType.FRIENDLY:
                        friendly_count += 1
                    elif unit.type == UnitType.ENEMY:
                        enemy_count += 1

                print("friendly: ", friendly_count, "enemy:", enemy_count)

                if friendly_count == 0:
                    # game over
                    self.mode = Main.GAME_OVER
                elif enemy_count == 0:
                    self.mode = Main.ROOM_FINISHED


        elif self.mode == Main.ROLLING:
            if ((self.renderer_counter % self.rate) == 0 and not self.paused):
                if not self.dice_roller is None:
                    try:
                        next(self.dice_roller)
                    except StopIteration as e:
                        pass


        self.renderer_counter += 1

        # drawing
        self.ctx.clear(1.0, 1.0, 1.0)
        self.ctx.enable(moderngl.DEPTH_TEST)
        self.ctx.enable(moderngl.BLEND)

        self.proj = Matrix44.perspective_projection(60.0, self.aspect_ratio, 0.1, 1000.0)
        self.lookat = Matrix44.look_at(
            (0.0, -10.0, 120.0),
            (0.0, 0.0, 0.0),
            (0.0, 1.0, 0.0)
        )

        self.light.value = (67.69, -8.14, 52.49)
        self.color.value = (1.0, 1.0, 1.0)
        self.use_texture.value = True

        if self.mode == Main.OVERVIEW:
            self.overview.draw(self.hexagon_vao, self.proj * self.lookat, self.mvp, self.use_texture, self.color, time)

        elif self.mode == Main.ROLLING or self.mode == Main.PLANNING or self.mode == Main.EXECUTING or self.mode == Main.ROOM_FINISHED:

            pixels = coord_helpers.flat_hex_to_pixel(self.dice_center, Room.hex_size)

            translate = Matrix44.from_translation([pixels[0], pixels[1], 0.])
            self.dice.draw(self.hexagon_vao, self.proj * self.lookat * translate, self.mvp, self.use_texture, self.color, time)

            self.room.draw(self.hexagon_vao, self.proj * self.lookat, self.mvp, self.use_texture, self.color)

            self.dice.draw_overlay(self.hexagon_vao, self.proj * self.lookat, self.mvp, self.use_texture, self.color, time)

            if self.mode == Main.PLANNING:
                self.use_texture.value = False

                #for unit, unit_plan in self.plan_loader.get_plan_friendly().unit_plans.items():
                #    unit_plan.draw.draw(self.hexagon_vao, self.proj * self.lookat, self.mvp, self.color)
                for unit, unit_plan in self.plan_friendly.unit_plans.items():
                    unit_plan.draw.draw(self.hexagon_vao, self.proj * self.lookat, self.mvp, self.color)


                #for unit, unit_plan in self.plan_loader.get_plan_enemy().unit_plans.items():
                #    unit_plan.draw.draw(self.hexagon_vao, self.proj * self.lookat, self.mvp, self.color)


        # draw text
        self.ortho_proj = Matrix44.orthogonal_projection(0.0, self.wnd.width, 0.0, self.wnd.height, 1.0, 10.0)
        self.ortho_translate = Matrix44.from_translation((300., 30., -1.0))
        self.ortho_scale = Matrix44.from_scale((10., 10., 1.))
        self.mvp.write((self.ortho_proj*self.ortho_translate*self.ortho_scale).astype('f4').tobytes())
        #self.ctx.disable(moderngl.DEPTH_TEST)
        #self.mvp.write((Matrix44()).astype('f4').tobytes())
        self.use_texture.value = True
        self.q_tex.use()
        #self.pipe_textures[6].use()
        #self.text_vao.render(moderngl.TRIANGLE_STRIP)
        #self.hexagon_vao.render(moderngl.TRIANGLE_FAN)

    def mouse_to_plane(self, x, y):
        width, height = self.wnd.size

        # taken from Anton Gerdelan link
        ray_clip = Vector4([(2. * x) / width - 1.0, 1.0 - (2. * y) / height, -1.0, 1.0])
        ray_eye = (~self.proj) * ray_clip
        ray_eye = Vector4([ray_eye.x, ray_eye.y, -1, 0.0])
        ray_wor = Vector3(((~self.lookat)*ray_eye).xyz)
        ray_wor.normalise()

        # this is the hexgrid plane normal
        surface_normal = Vector3([0., 0., 1.])
        surface_normal.normalise()
        #print(surface_normal)

        # print("ray = ", ray_wor)
        #
        # print("denom = ", (Vector3([0., 0., 0.]) | surface_normal))
        # print("numer = ", ray_wor | surface_normal)

        #d = self.distance_from_origin

        # this magic number is the camera distance from the hex grid (z axis)
        d = 120.
        t = -((Vector3([0., 0., 0.]) | surface_normal) + d) / (ray_wor | surface_normal)
        #print("t = ", t)
        point = (ray_wor*t)

        # this is the camera position (relative to the origin)
        point += Vector3([0., -10., 120.])
        return point

    def plane_to_coord(self, point):
        #print("point = ", point)
        # from Redblob hexagons
        #size = self.hex_radius
        size = Room.hex_size
        q = 2./3. * point.x / size
        r = -1./3. * point.x / size + np.sqrt(3.)/3. * point.y / size
        hex_key = coord_helpers.cube_round((q, r, -q-r))

        return hex_key

    def mouse_to_coord(self, x, y):
        point = self.mouse_to_plane(x, y)
        return self.plane_to_coord(point)

    def mouse_press_event(self, x: int, y: int, button: int):
        width, height = self.wnd.size

        coord = self.mouse_to_coord(x, y)

        # play sound
        print(coord)

        if self.mode == Main.OVERVIEW:
            self.overview.select(coord)

        elif self.mode == Main.PLANNING or self.mode == Main.ROLLING:
            self.dragging = True
            self.start_coord = coord
            self.last_coord = coord
            self.current_coords = []

        elif self.mode == Main.EXECUTING:
            try:
                cell = self.room.cells[coord]
            except:
                cell = None

            if not cell is None:
                print("Cell:", cell)
                print("Unit:", cell.unit)

    def mouse_release_event(self, x, y, button):
        if self.mode == Main.ROLLING:

            dice_coords = coord_helpers.cube_ring(self.dice_center, 2)

            if self.dragging and self.start_coord in dice_coords:
                player_coord = self.mouse_to_coord(x, y)

                if player_coord in self.room.cells:
                    unit = self.room.cells[player_coord].unit

                    if not unit is None and unit.type == UnitType.FRIENDLY:
                        dice_coord = coord_helpers.cube_add(self.start_coord,
                        (-self.dice_center[0], -self.dice_center[1], -self.dice_center[2]))
                        dice = self.dice.dice[dice_coord]
                        dice.set_unit(unit)

                        # if not self.plan.contains_unit_plan_for(unit):
                        #     unit_plan = UnitPlan(unit, player_coord, dice.number + 1)
                        #     self.plan.add_unit_plan(unit_plan)
                        # else:
                        #     self.unit_plan.num_moves = dice.number
                        # print(self.plan.get_unit_plan(unit))

        elif self.mode == Main.PLANNING:

            if self.dragging and self.start_coord in self.room.cells and\
                 not self.room.cells[self.start_coord].unit is None:
                 self.planning_selected_unit = self.room.cells[self.start_coord].unit

        self.dragging = False

    def mouse_position_event(self, x, y):
        if self.mode == Main.PLANNING:
            if self.dragging and self.start_coord in self.room.cells and\
                 not self.room.cells[self.start_coord].unit is None and\
                 self.room.cells[self.start_coord].unit.type == UnitType.FRIENDLY:

                    # point is raw x, y in world coordinates in plane of the hexagons
                    point = self.mouse_to_plane(x, y)
                    coord = self.plane_to_coord(point)

                    unit = self.room.cells[self.start_coord].unit
                    unit_plan = self.plan_friendly.get_unit_plan(unit)

                    self.planning_selected_unit = unit

                    if coord != self.last_coord:
                        print(coord, self.last_coord)
                        closest_direction = coord_helpers.closest_direction(coord_helpers.cube_subtract(coord, self.last_coord))
                        #print(Action.orient[closest_direction])


                        unit_plan.add_action(Action(Action.orient[closest_direction]))
                        unit_plan.add_action(Action(Action.MOVE))

                        self.last_coord = coord

                    # direction from angle between point and hex centre
                    hex_centre = coord_helpers.flat_hex_to_pixel(coord, Room.hex_size)

                    angle = np.arctan2(point.y - hex_centre[1], point.x - hex_centre[0])

                    if angle < -np.pi/3:
                        angle =  2.*np.pi + angle + np.pi/3
                    else:
                        angle = angle + np.pi/3

                    direction = int(angle/(np.pi/3))
                    print(orient_actions_reverse[direction])
                    unit_plan.add_action(Action(orient_actions_reverse[direction]))


            # unit_plan = self.plan.get_unit_plan_via_room(self.room, self.start_coord, create=True)
            #
            # if unit_plan is None:
            #     self.dragging = False
            #     return
            #
            # coord = self.mouse_to_coord(x, y)

    def key_event(self, key, action):
        #State - TOGGLE PAUSE
        if key == self.wnd.keys.P and action == self.wnd.keys.ACTION_PRESS:
            self.paused = not self.paused

        # State - ?
        elif key == self.wnd.keys.SPACE and action == self.wnd.keys.ACTION_PRESS:
            #State - Rolling dice
            if self.mode == Main.OVERVIEW and self.overview.selected is not None:

                self.generate_room()

                self.overview.enter_selected()
                self.mode = Main.ROLLING
                self.dice_roller = self.dice.roll()
            #State - Planning
            elif self.mode == Main.ROLLING and self.dice.is_done():
                self.plan_friendly = Plan()

                for coord, dice in self.dice.dice.items():
                    unit = dice.unit
                    if not unit is None:
                        self.plan_friendly.add_unit_plan(
                            UnitPlan(unit, unit.coord, dice.number+1)
                        )

                self.add_plan_draw(self.plan_friendly)

                self.mode = Main.PLANNING
            #State - Execution
            elif self.mode == Main.PLANNING:

                #Generate enemy plan
                print("Rolling for enemies...")
                #self.dice_roller = self.dice.roll()
                enemy_rolls = [random.randint(1, 6) for i in range(6)]
                print(enemy_rolls)
                self.plan_enemy = Plan.generate_enemy_plan(self.room, self.units, enemy_rolls)

                #Execute plans
                self.plan_executor = PlanExecutor(self.room, self.plan_friendly, self.plan_enemy)

                self.mode = Main.EXECUTING
            #State - finished execution
            elif self.mode == Main.EXECUTING and self.plan_executor.is_done():
                self.dice.clear()
                self.mode = Main.ROLLING
                self.load_next_plan()
                self.dice_roller = self.dice.roll()
            #State - Room completed
            elif self.mode == Main.ROOM_FINISHED:
                self.overview.current_completed()
                self.mode = Main.OVERVIEW

        #State - PLANNING
        if self.mode == Main.PLANNING:
            if action == self.wnd.keys.ACTION_PRESS:
                direction_keys = [self.wnd.keys.D, self.wnd.keys.E, self.wnd.keys.W, self.wnd.keys.Q, self.wnd.keys.A, self.wnd.keys.S]

                if key in direction_keys + [self.wnd.keys.UP, self.wnd.keys.F, self.wnd.keys.LEFT]:
                    unit_plan = self.plan_friendly.get_unit_plan(self.planning_selected_unit)

                if key in direction_keys:
                    action = getattr(Action, "ORIENT_%i" % direction_keys.index(key))
                    unit_plan.add_action(Action(action))
                elif key == self.wnd.keys.UP:
                    unit_plan.add_action(Action(Action.MOVE))
                elif key == self.wnd.keys.F:
                    unit_plan.add_action(Action(Action.ATTACK))
                elif key == self.wnd.keys.LEFT:
                    unit_plan.remove_action()

def main():
    run_example(Main)
